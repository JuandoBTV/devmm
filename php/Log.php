<?
    class Log{
        static public function write( $status, $message ){
            $date           = date("Y-m-d");
            $date_file_name = date("Y-m");
            $root_path      = dirname( __DIR__ , 2 ) + '/log/';
            $full_path_file = $root_path + $date_file_name + ".log";

            $file_exists = file_exists( $full_path_file );
            if(!$file_exists){
                $header_log = "";
                $header_log += "Date" + "\t\t\t" + "Status" + "\t\t" + "Description" + "\n";
                $header_log += "===================" + "\t" + "==========" + "\t" + "===============================================" + "\n";
                file_put_contents( $full_path_file, $header_log, FILE_APPEND | LOCK_EX );
            }

            $hora = date( "H:i:s" );
            file_put_contents( $full_path_file, $date + " " + $hora + "\t" + $status + "\t\t" + $message + "\n", FILE_APPEND | LOCK_EX )
        }
    }